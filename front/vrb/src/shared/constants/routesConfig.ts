import { AboutPage } from './../../app/components/pages/about/aboutComponent';
import { BlogPage } from "../../app/components/pages/blogComponent/blogComponent";
import { FaqPage } from "../../app/components/pages/faq/faqComponent";
import { HomePage } from "../../app/components/pages/home/HomePage";
import  LoginPage from "../../app/components/pages/login/LoginPage";
import RegistrationPage from "../../app/components/pages/registration/RegistrationPage";
import { ABOUT_ROUTE_PAGE, BLOG_ROUTE_PAGE, FAQ_ROUTE_PAGE, LOGIN_PAGE, REGISTRATION_PAGE, ROOT_ROUTE } from "./routes";

export type IRouteConfig = {
    path: string,
    component: any,
    exact: boolean
}


export const routerConfig: IRouteConfig[] = [
    {
        path: ROOT_ROUTE,
        component: HomePage,
        exact: true
    },
    {
        path: LOGIN_PAGE,
        component: LoginPage,
        exact: true
    },
    {
        path:REGISTRATION_PAGE,
        component: RegistrationPage,
        exact: true
    },
    {
        path: ABOUT_ROUTE_PAGE,
        component: AboutPage,
        exact: true
    },
    {
        path: FAQ_ROUTE_PAGE,
        component: FaqPage,
        exact: true
    },
    {
        path: BLOG_ROUTE_PAGE,
        component: BlogPage,
        exact: true
    }
];


export default routerConfig;
