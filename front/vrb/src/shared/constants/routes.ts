export const ROOT_ROUTE: string = "/home";

export const LOGIN_PAGE: string = "/auth";
export const REGISTRATION_PAGE: string = "/registration";
export const ABOUT_ROUTE_PAGE: string = "/home/about";
export const FAQ_ROUTE_PAGE: string = "/home/faq";
export const BLOG_ROUTE_PAGE: string = "/home/blog";