
export interface IServerErrors {
	message: string;
	errors?: {
		message: string,
		fieldName: string
	}[]
}
