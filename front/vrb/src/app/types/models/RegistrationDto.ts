export interface RegistrationDto {
    email: string,
    password: string,
    repeatePassword: string,
    username: string,
    isSubscribe: boolean,
    id?: string
}