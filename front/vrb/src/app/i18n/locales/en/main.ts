export const enTranslation: any  ={
    "Home": "Home",
    "About": "About",
    "FAQ": "FAQ",
    "Blog": "Blog",
    "Email": "Email",
    "Log in": "Log in",
    "DescriptionMainPage": "English text",
    "Sign in": "Sign in",
    "Terms and condition header": "By signing up, I agree to the ITFriend Terms & Conditions and",
    "Privacy policy": "Privacy Policy. I also confirm that I am at least 14 years of age.",
    "Full name": "Full name",
    "Password": "Password",
    "Repeat password": "Repeat password",
    "label": "I'd like to receive creative opportunities, event invites, and ITFriend news via email",
    "Agree and continue": "Agree and continue",
    "To Log In": "Log In"
}