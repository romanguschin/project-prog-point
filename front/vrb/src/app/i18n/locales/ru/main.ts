export const ruTranslation: any  ={
    "Home": "Главная",
    "About": "О Нас",
    "FAQ": "FAQ",
    "Blog": "Блог",
    "Email": "Email",
    "Log in": "Войти",
    "DescriptionMainPage": "Завлекаем людей, самая классная площадка, место, где каждый может найти себе команду и воплотить в жизнь свои идеи тралала",
    "Sign in": "Регистрация",
    "Terms and condition header" : "Регистрируясь, я соглашаюсь с условиями и политикой",
    "Privacy policy": "конфиденциальности ITFriends. Я также подтверждаю, что мне исполнилось 14 лет.",
    "Full name": "Имя пользователя",
    "Password": "Пароль",
    "Repeat password": "Повторите пароль",
    "label": "Я хочу получать предложения, приглашения на мероприятия и новости ITFriends по почте.",
    "Agree and continue": "Принять и продолжить",
    "To Log In": "Войти"
}