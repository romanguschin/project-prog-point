import "reflect-metadata";
import { RegistrationSaga } from './../components/pages/registration/RegistrationSaga';
import { NotificationService } from './../../shared/helpers/notificationService';
import { ObjectHelper } from './../services/objectHelper';
import { ApiHelper } from '../../shared/helpers/apiHelper';
import { ServerInteractionsUtils } from '../../shared/helpers/serverInteractionUtins';
import { AppActions } from '../appActions';
import { AppReducer } from '../appReducer';
import { AppSaga } from '../appSaga';
import { AccountServerInteractionService } from '../services/serverInteraction/accountServerInteractionService';
import { BaseServerInteractionService } from '../services/serverInteraction/baseServiceInteractionService';
import { TableServerInteractionService } from '../services/serverInteraction/tableServerInteractionService';
import { SERVICE_IDENTIFIER } from './inversifyTypes';
import { RegistrationActions } from '../components/pages/registration/RegistrationActions';
import { RegistrationReducer } from '../components/pages/registration/RegistrationReducer';
import { Container } from "inversify";

const container = new Container();

container.bind<BaseServerInteractionService>(SERVICE_IDENTIFIER.BaseServerInteractionService).to(BaseServerInteractionService);
container.bind<TableServerInteractionService>(SERVICE_IDENTIFIER.TableServerInteractionService).to(TableServerInteractionService);
container.bind<AccountServerInteractionService>(SERVICE_IDENTIFIER.AccountServerInteractionService).to(AccountServerInteractionService);


container.bind<ServerInteractionsUtils>(SERVICE_IDENTIFIER.ServerInteractionsUtils).to(ServerInteractionsUtils).inSingletonScope();
container.bind<NotificationService>(SERVICE_IDENTIFIER.NotificationService).to(NotificationService);

container.bind<ApiHelper>(SERVICE_IDENTIFIER.ApiHelper).to(ApiHelper);
container.bind<ObjectHelper>(SERVICE_IDENTIFIER.ObjectHelper).to(ObjectHelper);


container.bind<AppActions>(SERVICE_IDENTIFIER.AppActions).to(AppActions).inSingletonScope();
container.bind<RegistrationActions>(SERVICE_IDENTIFIER.RegistrationActions).to(RegistrationActions).inSingletonScope();

container.bind<AppSaga>(SERVICE_IDENTIFIER.AppSaga).to(AppSaga);
container.bind<RegistrationSaga>(SERVICE_IDENTIFIER.RegistrationSaga).to(RegistrationSaga);

container.bind<AppReducer>(SERVICE_IDENTIFIER.AppReducer).to(AppReducer);
container.bind<RegistrationReducer>(SERVICE_IDENTIFIER.RegistrationReducer).to(RegistrationReducer);


export default container;

