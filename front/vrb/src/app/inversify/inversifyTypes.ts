export const SERVICE_IDENTIFIER = {
    ServerInteractionsUtils: Symbol.for("ServerInteractionsUtils"),
    AccountServerInteractionService: Symbol.for("AccountServerInteractionService"),

    BaseServerInteractionService: Symbol.for("BaseServerInteractionService"),
    TableServerInteractionService: Symbol.for("TableServerInteractionService"),

    NotificationService: Symbol.for("NotificationService"),
    ObjectHelper: Symbol.for("ObjectHelper"),

    ApiHelper: Symbol.for("ApiHelper"),

    AppActions: Symbol.for("AppActions"),
    RegistrationActions: Symbol.for("RegistrationActions"),
   
    AppSaga: Symbol.for("AppSaga"),
    RegistrationSaga: Symbol.for("RegistrationSaga"),

    AppReducer: Symbol.for("AppReducer"),
    RegistrationReducer: Symbol.for("RegistrationReducer")
}