import { AppSaga } from './../appSaga';
import { RegistrationSaga } from './../components/pages/registration/RegistrationSaga';
import { all } from "@redux-saga/core/effects";
import container from "../inversify/inversifyContainer";
import { SERVICE_IDENTIFIER } from '../inversify/inversifyTypes';

export const rootSaga: () => Generator  = function* root(): Generator {
    yield all([
        container.get<RegistrationSaga>(SERVICE_IDENTIFIER.RegistrationSaga).watch(),
        container.get<AppSaga>(SERVICE_IDENTIFIER.AppSaga).watch()
    ]);
};