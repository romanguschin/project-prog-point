
import { IServerErrors } from "../../../types/IServerError";
import { IAction, IActionPayloaded } from "../shared/IAction";

export interface IBaseFormActions {
    initForm(): IAction;
    clearFormMessages(): IAction;
    setFormError(payload: IServerErrors): IActionPayloaded<IServerErrors>;
    setFormSuccess(message: string): IActionPayloaded<string>;
}