import { IRegistrationState } from './../components/pages/registration/RegisrationState';
import { RouterState } from "connected-react-router";
import { IAppState } from "./../IAppState";

export interface IRootState {
    router: RouterState;
    app: IAppState;
    registration: IRegistrationState;
}