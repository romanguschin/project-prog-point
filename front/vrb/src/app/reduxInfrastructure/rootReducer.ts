import { connectRouter } from "connected-react-router";
import { combineReducers } from "redux";
import { AppReducer } from "../appReducer";
import { RegistrationReducer } from "../components/pages/registration/RegistrationReducer";
import container from "../inversify/inversifyContainer";
import { SERVICE_IDENTIFIER } from "../inversify/inversifyTypes";

const registationReducer = container.get<RegistrationReducer>(SERVICE_IDENTIFIER.RegistrationReducer);
const appReducer = container.get<AppReducer>(SERVICE_IDENTIFIER.AppReducer);

export const rootReducer: (history: any) => any = (history: any): any => {
        return combineReducers({
                app: appReducer.getReducer(),
                registration: registationReducer.getReducer(),
                router: connectRouter(history),
        });
};