import React, { Component } from "react";
import './LoginPage.css'
import { TextInput, Button, Grid, Box, Grommet, Header } from 'grommet';
import { Hide, View } from 'grommet-icons'
import {customTheme} from '../../../../App'
import history from "../../../utils/helpers/HistoryHelper";
import { Translation } from "react-i18next";
import { REGISTRATION_PAGE } from "../../../../shared/constants/routes";

class LoginPage extends Component {
    state = { email: '', password: '', reveal: false };
    render() {
        const { email, password, reveal } = this.state;

        return (<Translation>
            {(t) => ( 
            <Grommet theme={customTheme}>
                <Header><h2 id='LoginHeader'>{t("Log in")}</h2></Header>
                <Box
                    direction="column"
                    align="center"
                    justify="center"
                    pad={{ horizontal: 'medium', vertical: 'small' }}
                    className="loginBody"
                >
                    <div>
                        <p className="loginText">{t("Email")}</p>
                        <TextInput
                            id='emailInput'
                            value={email}
                            onChange={event => this.setState({ email: event.target.value })}
                        />
                        <p className="loginText">{t("Password")}</p>

                        <Box
                            width="medium"
                            direction="row"
                            align="center"
                            round="xlarge"
                            border
                        >
                            <TextInput
                                id='passwordInput'
                                type={reveal ? 'text' : 'password'}
                                value={password}
                                onChange={event => this.setState({ password: event.target.value })}
                            />
                            <Button
                                icon={reveal ? <View size="medium" /> : <Hide size="medium" />}
                                onClick={() => this.setState({ reveal: !reveal })}
                            />

                        </Box>


                        <Grid
                            fill
                            margin={{ top: 'medium' }}
                            rows={['auto', 'auto']}
                            columns={['auto', 'auto']}
                            areas={[
                                { name: 'loginButton', start: [0, 0], end: [1, 0] },
                                { name: 'singinButton', start: [1, 0], end: [1, 1] },
                            ]}
                        >
                            <Box
                                gridArea="loginButton"
                                direction="column"
                                align="center"
                                justify="center"
                                pad={{ horizontal: 'medium', vertical: 'small' }}
                            >
                                <Button label={t("Log in")} />
                            </Box>

                            <Box
                                gridArea="singinButton"
                                direction="column"
                                align="center"
                                justify="center"
                                pad={{ horizontal: 'medium', vertical: 'small' }}
                            >
                                <Button onClick={() => history.push(REGISTRATION_PAGE)} label={t("Sign in")} />
                            </Box>
                        </Grid>
                    </div>
                </Box>
            </Grommet>
            )}
        </Translation>
        )
    }
}

export default LoginPage;