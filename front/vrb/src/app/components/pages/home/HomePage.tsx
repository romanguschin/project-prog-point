import React from "react";
import { menuConfig } from "../../../../shared/constants/menuItems";
import { REGISTRATION_PAGE } from "../../../../shared/constants/routes";
import { ButtonComponent } from "../../shared/buttons/buttonComponent";
import MenuCompnent from "../../menu/Menu";
import history from "../../../utils/helpers/HistoryHelper";
import './homePage.css'
import { useTranslation } from "react-i18next";

export type HomePageProps = {
	title: string
};

export const HomePage: (props: HomePageProps) => JSX.Element = (props: HomePageProps) => {
	const gotoSignUp = () => {
		history.push(REGISTRATION_PAGE);
	};
	const { t } = useTranslation(); 
	return (
		<>
			<MenuCompnent items={menuConfig} />
			<ButtonComponent onClick={() => { gotoSignUp() }} isValidating={false} formSuccess={""} type="button" className="tosignup__btn" value={t("Sign in")} />
			<div className="home__maintext">ITFriends<i className="home__maintext--point">.</i></div>
			<div className="home__motivatetext">
				{t("DescriptionMainPage")}
			</div>
		</>
	);
};
