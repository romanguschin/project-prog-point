import { injectable } from "inversify";
import { BaseServerInteractionService } from "./baseServiceInteractionService";

@injectable()
export class AccountServerInteractionService extends BaseServerInteractionService {
    public login = (payload: any): Promise<any> => {
        return this.post('/account/login', payload);
    }

    public getUser = (): Promise<any> => {
        return this.get('/account/self');
    }

    public logOut = (): Promise<any> => {
        return this.post('/account/logout');
    }

    public register = (payload: any): Promise<any> => {
        return this.post('/account/registration', payload);
    }
}