using System;

namespace ITFriends.Infrastructure.Domain.Common
{
    public enum Role
    {
        Admin = 1,
        Moderator = 2,
        Developer = 3,
        Investor = 4
    }

    public static class RoleConverter
    {
        public static string Convert(Role role)
        {
            return role switch
            {
                Role.Admin => "admin",
                Role.Developer => "developer",
                Role.Investor => "investor",
                Role.Moderator => "moderator",
                _ => throw new ArgumentOutOfRangeException(nameof(role), role, "Cannot convert role to string")
            };
        }
    }
}