﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using FluentValidation;
using ITFriends.Infrastructure.SeedWork.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ITFriends.Infrastructure.SeedWork.Errors
{
    public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
    {
        public int Order { get; } = int.MaxValue - 10;

        public void OnActionExecuting(ActionExecutingContext context) { }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            switch (context.Exception)
            {
                case BusinessLogicException businessLogicException:
                    context.Result = new ObjectResult(new BaseExceptionResponse<BusinessLogicError>(businessLogicException.Errors))
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest
                    };
                    context.ExceptionHandled = true;
                    return;
                
                case ValidationException validationException:
                    context.Result = new ObjectResult(new BaseExceptionResponse<ValidationError>(validationException.ValidationErrors()))
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest
                    };
                    context.ExceptionHandled = true;
                    return;
            }
        }
    }
}