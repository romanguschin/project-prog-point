﻿using System.Collections.Generic;

namespace ITFriends.Infrastructure.SeedWork.Errors
{
    public class BaseExceptionResponse<T> where T : IError
    {
        public List<T> Errors { get; }
        public string GlobalStatusCode { get; }

        public BaseExceptionResponse(List<T> errors)
        {
            GlobalStatusCode = typeof(T).Name;
            Errors = errors;
        }
    }
}