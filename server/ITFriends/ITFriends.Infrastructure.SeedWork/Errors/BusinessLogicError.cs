namespace ITFriends.Infrastructure.SeedWork.Errors
{
    public struct BusinessLogicErrors
    {
        public static readonly BusinessLogicError UserNotFoundError = new BusinessLogicError("UserNotFound", "User not found");
        public static readonly BusinessLogicError UserAlreadyRegisteredError = new BusinessLogicError("UserAlreadyRegistered", "User already registered");
        public static readonly BusinessLogicError CannotChangePasswordError = new BusinessLogicError("CannotChangePasswordError", "Cannot change password");
    }

    public class BusinessLogicError : IError
    {
        public string ErrorCode { get; }
        public string Message { get; }

        public BusinessLogicError(string errorCode, string message)
        {
            ErrorCode = errorCode;
            Message = message;
        }
    }
}