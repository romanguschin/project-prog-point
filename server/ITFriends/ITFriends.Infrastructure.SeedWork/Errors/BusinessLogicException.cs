using System;
using System.Collections.Generic;

namespace ITFriends.Infrastructure.SeedWork.Errors
{
    public class BusinessLogicException : Exception
    {
        public List<BusinessLogicError> Errors { get; }

        public BusinessLogicException(BusinessLogicError error, string message = null) : base(message)
        {
            Errors = new List<BusinessLogicError> {error};
        }
        
        public BusinessLogicException(List<BusinessLogicError> error, string message = null) : base(message)
        {
            Errors = error;
        }
    }
}