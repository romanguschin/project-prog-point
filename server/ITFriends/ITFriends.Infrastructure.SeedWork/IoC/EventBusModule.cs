﻿using Autofac;
using ITFriends.Infrastructure.EventBus;
using ITFriends.Infrastructure.EventBus.RabbitMQ;
using RabbitMQ.Client;

namespace ITFriends.Infrastructure.SeedWork.IoC
{
    public class EventBusModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RabbitMQPersistentConnection>()
                .As<IPersistentConnection<IModel>>()
                .SingleInstance();

            builder.RegisterType<RabbitMQEventBus>()
                .As<IEventBus>()
                .SingleInstance();

            builder.RegisterType<EventDispatcher>()
                .As<IEventDispatcher>()
                .SingleInstance();
        }
    }
}