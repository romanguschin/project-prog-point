﻿
using Microsoft.AspNetCore.Identity;

namespace ITFriends.Infrastructure.Domain.Write
{
    public class AppUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}