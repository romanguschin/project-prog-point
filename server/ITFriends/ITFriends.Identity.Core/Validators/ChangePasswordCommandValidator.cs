﻿using FluentValidation;
using ITFriends.Identity.Core.Commands;
using ITFriends.Infrastructure.SeedWork;
using ITFriends.Infrastructure.SeedWork.Validator;
using Microsoft.AspNetCore.Identity;

namespace ITFriends.Identity.Core.Validators
{
    public class ChangePasswordCommandValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCommandValidator()
        {
            RuleFor(x => x.UserId).NotNull().NotEmpty();
            RuleFor(x => x.OldPassword).NotNull().NotEmpty();
            RuleFor(x => x.NewPassword).SetValidator(new PasswordValidator());
            RuleFor(x => x.NewConfirmPassword).NotNull().NotEmpty()
                .Equal(x => x.NewPassword);
        }
    }
}