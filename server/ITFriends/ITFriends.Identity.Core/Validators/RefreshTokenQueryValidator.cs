﻿using FluentValidation;
using ITFriends.Identity.Core.Queries;

namespace ITFriends.Identity.Core.Validators
{
    public class RefreshTokenQueryValidator : AbstractValidator<RefreshTokenQuery>
    {
        public RefreshTokenQueryValidator()
        {
            RuleFor(x => x.RefreshToken).NotNull().NotEmpty();
        }
    }
}