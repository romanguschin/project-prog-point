﻿using FluentValidation;
using ITFriends.Identity.Core.Commands;
using ITFriends.Infrastructure.SeedWork.Validator;

namespace ITFriends.Identity.Core.Validators
{
    public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
    {
        public RegisterCommandValidator()
        {
            RuleFor(c => c.UserName)
                .NotNull()
                .MinimumLength(3)
                .MaximumLength(32);

            RuleFor(c => c.FirstName)
                .NotNull()
                .NotEmpty()
                .MaximumLength(64);

            RuleFor(c => c.LastName)
                .NotNull()
                .NotEmpty()
                .MaximumLength(64);

            RuleFor(c => c.Email)
                .NotNull()
                .EmailAddress();

            RuleFor(c => c.Password).NotNull()
                .SetValidator(new PasswordValidator());

            RuleFor(c => c.ConfirmPassword)
                .NotNull()
                .Equal(c => c.Password);

            RuleFor(c => c.Role)
                .NotNull();
        }
    }
}