using System;
using ITFriends.Infrastructure.Domain.Common;
using MediatR;

namespace ITFriends.Identity.Core.Commands
{
    public class RegisterCommand : IRequest<Guid>
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public Role Role { get; set; }
    }
}