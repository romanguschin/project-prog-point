﻿using System.Collections.Generic;
using ITFriends.Infrastructure.Domain.Write;
using ITFriends.Infrastructure.EventBus;

namespace ITFriends.Identity.Core.Events
{
    public class UserCreatedEvent : Event
    {
        public AppUser CreatedUser { get; }
        public List<string> Roles { get; }

        public UserCreatedEvent(AppUser createdUser, List<string> roles)
        {
            CreatedUser = createdUser;
            Roles = roles;
        }
    }
}