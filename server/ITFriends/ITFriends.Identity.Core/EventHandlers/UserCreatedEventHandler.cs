﻿using System.Threading.Tasks;
using ITFriends.Identity.Core.Events;
using ITFriends.Infrastructure.Data.Repositories.Read;
using ITFriends.Infrastructure.Domain.Read;
using ITFriends.Infrastructure.EventBus;

namespace ITFriends.Identity.Core.EventHandlers
{
    public class UserCreatedEventHandler : IEventHandler<UserCreatedEvent>
    {
        private readonly IAppUserReadRepository _appUserReadRepository;

        public UserCreatedEventHandler(IAppUserReadRepository appUserReadRepository)
        {
            _appUserReadRepository = appUserReadRepository;
        }
        
        public async Task Handle(UserCreatedEvent e)
        {
            var readUser = new AppUser
            {
                Id = e.CreatedUser.Id,
                FistName = e.CreatedUser.FirstName,
                LastName = e.CreatedUser.LastName,
                UserName = e.CreatedUser.UserName,
                Email = e.CreatedUser.Email,
                EmailConfirmed = false,
                Roles = e.Roles
            };

            await _appUserReadRepository.Insert(readUser);
        }
    }
}