﻿using ITFriends.Identity.Core.Dto;
using MediatR;

namespace ITFriends.Identity.Core.Queries
{
    public class RefreshTokenQuery : IRequest<TokenDto>
    {
        public string RefreshToken { get; set; }
    }
}