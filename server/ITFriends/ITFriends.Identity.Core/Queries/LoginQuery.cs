using System;
using System.Net.Mail;
using FluentValidation;
using ITFriends.Identity.Core.Dto;
using MediatR;

namespace ITFriends.Identity.Core.Queries
{
    public class LoginQuery : IRequest<TokenDto>
    {
        public string Username { get; set; }
        public string Password { get; set; }

    }
}