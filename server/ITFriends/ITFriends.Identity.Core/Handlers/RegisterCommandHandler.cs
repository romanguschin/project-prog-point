﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using ITFriends.Identity.Core.Commands;
using ITFriends.Identity.Core.Events;
using ITFriends.Infrastructure.Domain.Common;
using ITFriends.Infrastructure.Domain.Write;
using ITFriends.Infrastructure.EventBus;
using ITFriends.Infrastructure.SeedWork.Errors;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace ITFriends.Identity.Core.Handlers
{
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, Guid>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEventBus _eventBus;

        public RegisterCommandHandler(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager
            , IEventBus eventBus)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _eventBus = eventBus;
        }

        public async Task<Guid> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByNameAsync(request.UserName);
            
            if (user != null)
                throw new BusinessLogicException(BusinessLogicErrors.UserAlreadyRegisteredError);
            
            var guid = Guid.NewGuid();
            
            var newUser = new AppUser
            {
                Id = guid.ToString(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                UserName = request.UserName,
                EmailConfirmed = false
            };
            
            var result = await _userManager.CreateAsync(newUser, request.Password);
            
            if (!result.Succeeded)
            {
                throw new Exception("Cannot create user");
            }

            var role = RoleConverter.Convert(request.Role);
            
            await SaveUserRole(newUser, role);
            await SaveUserClaims(newUser, role);

            var savedUser = await _userManager.FindByNameAsync(newUser.UserName);

            _eventBus.Publish(new UserCreatedEvent(savedUser, new List<string> {role}));
            
            return guid;
        }
        
        private async Task SaveUserRole(AppUser user, string role)
        {
            if (!await _roleManager.RoleExistsAsync(role))
                await _roleManager.CreateAsync(new IdentityRole(role));

            await _userManager.AddToRoleAsync(user, role);
        }

        private async Task SaveUserClaims(AppUser user, string role)
        {
            var claims = new List<Claim>
            {
                new Claim("username", user.UserName),
                new Claim("email", user.Email),
                new Claim("firstname", user.FirstName),
                new Claim("lastname", user.LastName),
                new Claim("role", role)
            };
        
            await _userManager.AddClaimsAsync(user, claims);
        }
    }
}