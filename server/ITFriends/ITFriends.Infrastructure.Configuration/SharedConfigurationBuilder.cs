﻿using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace ITFriends.Infrastructure.Configuration
{
    public static class SharedConfigurationBuilder
    {
        public static void Configure(HostBuilderContext hostBuilderContext, IConfigurationBuilder configurationBuilder)
        {
            var env = hostBuilderContext.HostingEnvironment;
            var sharedFolder = Path.Combine(env.ContentRootPath, "..", "ITFriends.Infrastructure.Configuration");

            configurationBuilder
                .AddJsonFile(Path.Combine(sharedFolder, "SharedConfiguration.json"), true)                         // When running using dotnet run
                .AddJsonFile(Path.Combine(sharedFolder, $"SharedConfiguration.{env.EnvironmentName}.json"), true)  // When running using dotnet run
                .AddJsonFile("SharedConfiguration.json", true)                                                // When app is published
                .AddJsonFile($"SharedConfiguration.{env.EnvironmentName}.json", true)                         // When app is published
                .AddJsonFile("appsettings.json", true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true);

            configurationBuilder.AddEnvironmentVariables();
        }
    }
}