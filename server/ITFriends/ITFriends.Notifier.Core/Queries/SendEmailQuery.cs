﻿using MediatR;

namespace ITFriends.Notifier.Core.Queries
{
    public class SendEmailQuery : IRequest<Unit>
    {
        public string ReceiverEmail { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public bool IsBodyHtml { get; set; }
    }
}