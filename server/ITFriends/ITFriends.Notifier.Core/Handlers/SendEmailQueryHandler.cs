﻿using System.Threading;
using System.Threading.Tasks;
using ITFriends.Notifier.Core.Queries;
using ITFriends.Notifier.Core.Services;
using MediatR;

namespace ITFriends.Notifier.Core.Handlers
{
    public class SendEmailQueryHandler : IRequestHandler<SendEmailQuery, Unit>
    {
        private readonly IEmailSender _emailSender;

        public SendEmailQueryHandler(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public async Task<Unit> Handle(SendEmailQuery request, CancellationToken cancellationToken)
        {
            await _emailSender.SendEmail(request.ReceiverEmail, request.Body, request.Subject, request.IsBodyHtml);
            
            return Unit.Value;
        }
    }
}