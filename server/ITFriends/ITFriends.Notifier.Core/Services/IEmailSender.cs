﻿using System.Threading.Tasks;

namespace ITFriends.Notifier.Core.Services
{
    public interface IEmailSender
    {
        public Task SendEmail(string receiverEmail, string body, string subject, bool isBodyHtml);
    }
}