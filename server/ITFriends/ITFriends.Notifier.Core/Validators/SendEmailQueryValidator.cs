﻿using FluentValidation;
using ITFriends.Notifier.Core.Queries;

namespace ITFriends.Notifier.Core.Validators
{
    public class SendEmailQueryValidator : AbstractValidator<SendEmailQuery>
    {
        public SendEmailQueryValidator()
        {
            RuleFor(x => x.ReceiverEmail).NotNull().EmailAddress();
            RuleFor(x => x.Body).NotNull();
            RuleFor(x => x.Subject).NotNull();
        }
    }
}