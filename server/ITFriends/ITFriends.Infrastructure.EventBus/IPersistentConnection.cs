﻿using System;

namespace ITFriends.Infrastructure.EventBus
{
    public interface IPersistentConnection<out T> : IDisposable
    {
        bool IsConnected { get; }
        bool TryConnect();
        T CreateModel();
    }
}