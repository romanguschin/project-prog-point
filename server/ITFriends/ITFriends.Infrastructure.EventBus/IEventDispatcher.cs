﻿using System.Threading.Tasks;

namespace ITFriends.Infrastructure.EventBus
{
    public interface IEventDispatcher
    {
        Task Dispatch<TEvent>(TEvent e) where TEvent : IEvent;
    }
}