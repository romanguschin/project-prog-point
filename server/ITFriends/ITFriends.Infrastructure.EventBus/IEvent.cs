﻿using System;

namespace ITFriends.Infrastructure.EventBus
{
    public interface IEvent
    {
        Guid Id { get; set; }
        DateTime CreationDate { get; set; }
    }
}