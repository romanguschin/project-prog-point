﻿using System.Threading.Tasks;

namespace ITFriends.Infrastructure.EventBus
{
    public interface IEventHandler<in TEvent> where TEvent : IEvent
    {
        Task Handle(TEvent e);
    }
}