﻿using System;

namespace ITFriends.Infrastructure.EventBus
{
    public abstract class Event : IEvent
    {
        protected Event()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.Now;
        }
        
        public Guid Id { get; set; }
        public DateTime CreationDate { get; set; }
    }
}