﻿namespace ITFriends.Infrastructure.EventBus.RabbitMQ
{
    public class RabbitMQConfiguration
    {
        public string Host { get; set; }
        public string VHost { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public int RetryCount { get; set; }
        public string QueueName { get; set; }
        public string BrokerName { get; set; }
    }
}