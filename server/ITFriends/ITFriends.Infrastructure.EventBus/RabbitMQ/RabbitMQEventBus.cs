﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Polly;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace ITFriends.Infrastructure.EventBus.RabbitMQ
{
    public class RabbitMQEventBus : IEventBus, IDisposable
    {
        private readonly string _queueName;
        private readonly string _brokerName;

        private readonly IEventDispatcher _eventDispatcher;
        private readonly IPersistentConnection<IModel> _persistentConnection;
        private readonly ILogger<RabbitMQEventBus> _logger;
        private static readonly Dictionary<string, Type> SubsManager = new Dictionary<string, Type>();
        private readonly int _retryCount;

        private IModel _consumerChannel;

        public RabbitMQEventBus(IEventDispatcher eventDispatcher,
            IPersistentConnection<IModel> persistentConnection,
            ILogger<RabbitMQEventBus> logger,
            RabbitMQConfiguration configuration)
        {
            _eventDispatcher = eventDispatcher ?? throw new ArgumentNullException(nameof(eventDispatcher));
            _persistentConnection = persistentConnection ?? throw new ArgumentNullException(nameof(persistentConnection));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _retryCount = configuration.RetryCount;
            _queueName = configuration.QueueName;
            _brokerName = configuration.BrokerName;
            _consumerChannel = CreateConsumerChannel();

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new NonPublicPropertiesResolver()
            };
        }

        public void Publish(IEvent @event)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var policy = Policy.Handle<BrokerUnreachableException>()
                .Or<SocketException>()
                .WaitAndRetry(_retryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                {
                    _logger.LogWarning(ex.ToString());
                });

            using var channel = _persistentConnection.CreateModel();
            var eventName = @event.GetType().Name;

            channel.ExchangeDeclare(_brokerName,
                "direct");

            var message = JsonConvert.SerializeObject(@event);
            var body = Encoding.UTF8.GetBytes(message);

            policy.Execute(() =>
            {
                // ReSharper disable once AccessToDisposedClosure
                var properties = channel.CreateBasicProperties();
                properties.DeliveryMode = 2; // persistent

                // ReSharper disable once AccessToDisposedClosure
                channel.BasicPublish(_brokerName,
                    eventName,
                    true,
                    properties,
                    body);
            });
        }

        private class NonPublicPropertiesResolver : DefaultContractResolver
        {
            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                var prop = base.CreateProperty(member, memberSerialization);
                if (!(member is PropertyInfo pi)) return prop;
                prop.Readable = (pi.GetMethod != null);
                prop.Writable = (pi.SetMethod != null);
                return prop;
            }
        }

        public void Subscribe<T>() where T : IEvent
        {
            var eventName = typeof(T).Name;
            var containsKey = SubsManager.ContainsKey(eventName);
            if (!containsKey)
            {
                SubsManager.Add(eventName, typeof(T));
            }

            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            using var channel = _persistentConnection.CreateModel();
            channel.QueueBind(_queueName,
                _brokerName,
                eventName);
        }

        private IModel CreateConsumerChannel()
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var channel = _persistentConnection.CreateModel();

            channel.ExchangeDeclare(_brokerName,
                                 "direct");

            channel.QueueDeclare(_queueName,
                                 true,
                                 false,
                                 false,
                                 null);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                var eventName = ea.RoutingKey;
                var message = Encoding.UTF8.GetString(ea.Body.ToArray());

                await ProcessEvent(eventName, message);

                channel.BasicAck(ea.DeliveryTag, false);
            };

            channel.BasicConsume(_queueName,
                                 false,
                                 consumer);

            channel.CallbackException += (sender, ea) =>
            {
                _consumerChannel.Dispose();
                _consumerChannel = CreateConsumerChannel();
            };

            return channel;
        }

        private async Task ProcessEvent(string eventName, string message)
        {
            if (SubsManager.ContainsKey(eventName))
            {
                var type = SubsManager[eventName];
                var @event = JsonConvert.DeserializeObject(message, type) as IEvent;

                await _eventDispatcher.Dispatch(@event);
            }
        }

        private bool _disposedValue; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (_disposedValue) return;
            if (disposing)
            {
                _consumerChannel?.Dispose();

                SubsManager.Clear();
            }
            _disposedValue = true;
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }
    }
}