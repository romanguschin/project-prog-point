﻿using System.Threading.Tasks;
using ITFriends.Infrastructure.Domain.Read;

namespace ITFriends.Infrastructure.Data.Repositories.Read
{
    public interface IAppUserReadRepository
    {
        Task Insert(AppUser user);
    }
}