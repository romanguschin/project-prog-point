﻿using System.Threading.Tasks;
using MongoDB.Driver;
using ITFriends.Infrastructure.Domain.Read;

namespace ITFriends.Infrastructure.Data.Repositories.Read
{
    public class AppUserReadRepository : IAppUserReadRepository
    {
        private readonly IMongoCollection<AppUser> _appUsers;

        public AppUserReadRepository(IMongoDatabase database)
        {
            _appUsers = database.GetCollection<AppUser>("AppUsers");
        }

        public async Task Insert(AppUser user)
        {
            await _appUsers.InsertOneAsync(user);
        }
    }
}