﻿using ITFriends.Infrastructure.Domain.Write;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ITFriends.Infrastructure.Data.Contexts
{
    public class AppUserWriteDbContext : IdentityDbContext<AppUser>
    {
        public AppUserWriteDbContext(DbContextOptions<AppUserWriteDbContext> options)
            : base(options)
        {

        }
    }
}