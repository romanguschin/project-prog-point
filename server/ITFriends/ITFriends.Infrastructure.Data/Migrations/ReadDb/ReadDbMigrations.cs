﻿using ITFriends.Infrastructure.Domain.Read;
using MongoDB.Driver;

namespace ITFriends.Infrastructure.Data.Migrations.ReadDb
{
    public static class ReadDbMigrations
    {
        public static void ExecuteMigrations(IMongoDatabase database)
        {
            var collection = database.GetCollection<AppUser>("AppUsers");

            var userNameIndex = Builders<AppUser>.IndexKeys.Ascending(user => user.UserName);
            var emailIndex = Builders<AppUser>.IndexKeys.Ascending(user => user.Email);

            collection.Indexes.CreateOne(new CreateIndexModel<AppUser>(userNameIndex));
            collection.Indexes.CreateOne(new CreateIndexModel<AppUser>(emailIndex));
        }
    }
}