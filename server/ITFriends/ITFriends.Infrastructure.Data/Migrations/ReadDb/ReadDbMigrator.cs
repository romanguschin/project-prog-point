﻿using System;
using DnsClient.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;

namespace ITFriends.Infrastructure.Data.Migrations.ReadDb
{
    public static class ReadDbMigrator
    {
        public static void RunMigrate(IHost host)
        {
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;

            try
            {
                var context = services.GetRequiredService<IMongoDatabase>();
                ReadDbMigrations.ExecuteMigrations(context);
            }
            catch (Exception ex)
            {
                var logger = services.GetRequiredService<ILogger>();
                logger.LogError(ex, "An error occurred creating the read DB");
            }
        }
    }
}