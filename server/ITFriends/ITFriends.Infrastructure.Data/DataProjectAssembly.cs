﻿namespace ITFriends.Infrastructure.Data
{
    public static class DataProjectAssembly
    {
        public static string Assembly => typeof(DataProjectAssembly).Assembly.GetName().Name;
    }
}