using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using ITFriends.Identity.Api.IoC;
using ITFriends.Identity.Core;
using ITFriends.Identity.Core.Events;
using ITFriends.Infrastructure.Configuration;
using ITFriends.Infrastructure.Data.Contexts;
using ITFriends.Infrastructure.Domain.Write;
using ITFriends.Infrastructure.EventBus;
using ITFriends.Infrastructure.EventBus.RabbitMQ;
using ITFriends.Infrastructure.SeedWork.Errors;
using ITFriends.Infrastructure.SeedWork.IoC;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ITFriends.Identity.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public IConfiguration Configuration { get; }
        public ILifetimeScope AutofacContainer { get; private set; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            var writeDbConnectionString = Configuration.GetConnectionString("WriteDb");
            
            services.AddDbContext<AppUserWriteDbContext>(config =>
            {
                config.UseSqlServer(writeDbConnectionString);
            });

            services.AddIdentity<AppUser, IdentityRole>(config =>
                {
                    config.Password.RequiredLength = 4;
                    config.Password.RequireDigit = false;
                    config.Password.RequireNonAlphanumeric = false;
                    config.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<AppUserWriteDbContext>() // UserManager and etc use write db
                .AddDefaultTokenProviders();
            
            services.AddControllers(options =>
            {
                options.Filters.Add(new HttpResponseExceptionFilter());
            });
            
            services.AddAuthentication(options => 
                {    
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = Configuration["IS4Url"];
                    options.RequireHttpsMetadata = false;
                    options.ApiName = Configuration["IS4Scope"];
                });
            
            services.AddSwaggerGen(x => x.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "ITFriends.Identity API", Version = "v1" }));
        }
        
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new MediatorModule(IdentityCoreProjectAssembly.Assembly));
            builder.RegisterModule(new EventBusModule());
            builder.RegisterModule(new EventModule());
            builder.RegisterModule(new ReadRepositoryModule());
            builder.RegisterAutoMapper(typeof(Startup).Assembly);
            builder.Register(_ => Configuration.GetSection("RabbitMQ").Get<RabbitMQConfiguration>());
            builder.Register(_ => Configuration.GetSection("ReadDbConfiguration").Get<ReadDbConfiguration>());
        }

        private void ConfigureEventBus(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            
            eventBus.Subscribe<UserCreatedEvent>();
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            AutofacContainer = app.ApplicationServices.GetAutofacRoot();
            
            app.UseCors(builder =>
                builder
                    .WithOrigins(Configuration["ClientUrl"])
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials()
            );
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            
            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("v1/swagger.json", "IT Friends");
            });

            app.UseSwagger();
            
            ConfigureEventBus(app);
        }
    }
}
