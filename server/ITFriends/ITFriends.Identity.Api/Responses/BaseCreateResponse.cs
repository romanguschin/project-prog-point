namespace Identity.API.Responses
{
    public class BaseCreateResponse
    {
        public long CreatedId { get; set; }

        public BaseCreateResponse(long createdId)
        {
            CreatedId = createdId;
        }
    }
}