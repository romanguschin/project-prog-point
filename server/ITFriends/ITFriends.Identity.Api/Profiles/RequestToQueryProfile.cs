﻿using AutoMapper;
using ITFriends.Identity.Api.Requests;
using ITFriends.Identity.Core.Queries;

namespace ITFriends.Identity.Api.Profiles
{
    public class RequestToQueryProfile : Profile
    {
        public RequestToQueryProfile()
        {
            CreateMap<LoginRequest, LoginQuery>();
            CreateMap<RefreshTokenRequest, RefreshTokenQuery>();
        }
    }
}