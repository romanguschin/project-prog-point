﻿using AutoMapper;
using ITFriends.Identity.Api.Requests;
using ITFriends.Identity.Core.Commands;

namespace ITFriends.Identity.Api.Profiles
{
    public class RequestToCommandProfile : Profile
    {
        public RequestToCommandProfile()
        {
            CreateMap<RegisterRequest, RegisterCommand>();
            CreateMap<ChangePasswordRequest, ChangePasswordCommand>();
        }
    }
}