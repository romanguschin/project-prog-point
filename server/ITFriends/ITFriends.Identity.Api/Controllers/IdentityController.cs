﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using ITFriends.Identity.Api.Requests;
using ITFriends.Identity.Api.Responses;
using ITFriends.Identity.Core.Commands;
using ITFriends.Identity.Core.Queries;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITFriends.Identity.Api.Controllers
{
    [Authorize]
    public class IdentityController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public IdentityController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterRequest request)
        {
            var command = _mapper.Map<RegisterCommand>(request);
            
            var result = await _mediator.Send(command);
        
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var query = _mapper.Map<LoginQuery>(request);

            var result = await _mediator.Send(query);

            var response = _mapper.Map<TokenResponse>(result);
            
            if (response.HasError)
                return Unauthorized(response);

            return Ok(response);
        }
        
        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();

            return Ok();
        }

        [HttpPost("token/refresh")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest request)
        {
            var query = _mapper.Map<RefreshTokenQuery>(request);

            var result = await _mediator.Send(query);
            
            var response = _mapper.Map<TokenResponse>(result);
            
            if (response.HasError)
                return Unauthorized(response);

            return Ok(response);
        }
        
        [HttpPost("password/change")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordRequest request)
        {
            var command = _mapper.Map<ChangePasswordCommand>(request);
            command.UserId = User.FindFirst("sub").Value;

            await _mediator.Send(command);

            return Ok();
        }
        
        [HttpPost("password/restore")]
        public async Task<IActionResult> RestorePassword()
        {
            throw new NotImplementedException();
        }
    }
}