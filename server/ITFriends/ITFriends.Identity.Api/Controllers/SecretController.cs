﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ITFriends.Identity.Api.Controllers
{
    [Authorize]
    public class SecretController : ControllerBase
    {
        [HttpGet]
        [Route("secret")]
        public IActionResult Get()
        {
            return new JsonResult(from c in User.Claims select new {c.Type, c.Value});
        }

        [HttpGet]
        [Route("superpowers")]
        [Authorize(Roles = "admin")]
        public IActionResult Superpowers()
        {
            return new JsonResult("Superpowers!");
        }
    }
}