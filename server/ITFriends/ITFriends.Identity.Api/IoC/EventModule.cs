﻿using Autofac;
using ITFriends.Identity.Core.EventHandlers;
using ITFriends.Identity.Core.Events;
using ITFriends.Infrastructure.EventBus;

namespace ITFriends.Identity.Api.IoC
{
    public class EventModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IEventHandler<>).Assembly)
                .AsClosedTypesOf(typeof(IEventHandler<>))
                .InstancePerLifetimeScope();
            
            // TODO try register automatically
            builder.RegisterType<UserCreatedEventHandler>()
                .As<IEventHandler<UserCreatedEvent>>()
                .InstancePerLifetimeScope();
        }
    }
}