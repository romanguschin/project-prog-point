﻿using AutoMapper;
using ITFriends.Notifier.Api.Requests;
using ITFriends.Notifier.Core.Queries;

namespace ITFriends.Notifier.Api.Profiles
{
    public class RequestToQueryProfile : Profile
    {
        public RequestToQueryProfile()
        {
            CreateMap<SendEmailRequest, SendEmailQuery>();
        }
    }
}