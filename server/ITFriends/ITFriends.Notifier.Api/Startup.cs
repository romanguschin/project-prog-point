using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using ITFriends.Infrastructure.Configuration;
using ITFriends.Infrastructure.Data.Contexts;
using ITFriends.Infrastructure.EventBus;
using ITFriends.Infrastructure.EventBus.RabbitMQ;
using ITFriends.Infrastructure.SeedWork.Errors;
using ITFriends.Infrastructure.SeedWork.IoC;
using ITFriends.Notifier.Core;
using ITFriends.Notifier.Core.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ITFriends.Notifier.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public ILifetimeScope AutofacContainer { get; private set; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            var writeDbConnectionString = Configuration.GetConnectionString("WriteDb");
            
            services.AddDbContext<AppUserWriteDbContext>(config =>
            {
                config.UseSqlServer(writeDbConnectionString);
            });
            
            services.AddControllers(options =>
            {
                options.Filters.Add(new HttpResponseExceptionFilter());
            });
            
            services.AddSwaggerGen(x => x.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "ITFriends.Notifier API", Version = "v1" }));
        }
        
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new MediatorModule(NotifierCoreProjectAssembly.Assembly));
            builder.RegisterModule(new EventBusModule());
            // builder.RegisterModule(new EventModule());
            builder.RegisterModule(new ReadRepositoryModule());
            builder.RegisterModule(new ToolsModule());
            builder.RegisterAutoMapper(typeof(Startup).Assembly);
            builder.Register(_ => Configuration.GetSection("RabbitMQ").Get<RabbitMQConfiguration>());
            builder.Register(_ => Configuration.GetSection("ReadDbConfiguration").Get<ReadDbConfiguration>());
            builder.Register(_ => Configuration.GetSection("SmtpClient").Get<SmtpClientConfiguration>());
            builder.RegisterType<SmtpEmailSender>().As<IEmailSender>();
        }
        
        private void ConfigureEventBus(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            
            // eventBus.Subscribe<UserCreatedEvent>();
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            AutofacContainer = app.ApplicationServices.GetAutofacRoot();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            
            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("v1/swagger.json", "IT Friends");
            });

            app.UseSwagger();
            
            ConfigureEventBus(app);
        }
    }
}