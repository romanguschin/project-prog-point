﻿namespace ITFriends.Notifier.Api.Requests
{
    public class SendEmailRequest
    {
        public string ReceiverEmail { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public bool IsBodyHtml { get; set; } = false;
    }
}