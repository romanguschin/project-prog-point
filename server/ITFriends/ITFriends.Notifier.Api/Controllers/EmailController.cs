﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using ITFriends.Infrastructure.EventBus;
using ITFriends.Notifier.Api.Requests;
using ITFriends.Notifier.Core;
using ITFriends.Notifier.Core.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace ITFriends.Notifier.Api.Controllers
{
    public class EmailController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        
        public EmailController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }
        
        [HttpPost("email/send")]
        public async Task<IActionResult> SendEmail([FromBody] SendEmailRequest request)
        {
            var query = _mapper.Map<SendEmailQuery>(request);

            await _mediator.Send(query);

            return Ok();
        }
    }
}